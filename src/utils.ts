export const scrollToElement = (element: string) => () => {
  document
    .getElementById(element)!
    .scrollIntoView({ block: 'start', behavior: 'smooth' });
};
