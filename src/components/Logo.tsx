import React from 'react';

const logo = () => <img src={require('../../assets/logo.svg')} />;

export default logo;
