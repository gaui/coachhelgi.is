import React from 'react';
import { scrollToElement } from '../utils';

const Section = (props: any) => {
  const div =
    typeof props.children === 'string' ? (
      <div id={props.id} dangerouslySetInnerHTML={{ __html: props.children }} />
    ) : (
      <div id={props.id}>
        <h1>{props.header}</h1>
        {props.children}
      </div>
    );

  return (
    <div className="content-area">
      {div}
      <div
        className="fas fa-chevron-up fa-3x"
        onClick={scrollToElement('root')}
      />
    </div>
  );
};

export default Section;
