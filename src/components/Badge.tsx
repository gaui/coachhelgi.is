import React from 'react';

const Icon = ({ icon, link, ...rest }: any) => (
  <a href={link} {...rest}>
    <i className={icon} />
  </a>
);

export default Icon;
