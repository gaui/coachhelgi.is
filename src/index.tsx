import '@fortawesome/fontawesome-free/css/all.css';
import React from 'react';
import { render } from 'react-dom';
import 'scroll-behavior-polyfill';
import App from './components/App';

render(<App />, document.getElementById('root'));
